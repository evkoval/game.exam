#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>


namespace ek
{
	class Circle
	{
	public:
		Circle(float r, int x, int y, float velocity);
		
		~Circle();

		sf::CircleShape* Get();

		void Move();

		void MoveKorzR();

		void MoveKorzL();

		void SetY(int y);

		int GetY();

		int GetX();

		float GetR();


	private:
		int m_x, m_y;
		float m_r;
		float m_velocity;
		sf::CircleShape* m_circle;

	};


	class Circle2
	{
	public:
		Circle2(float r, int x, int y, float velocity);

		~Circle2();

		sf::Sprite* Get();

		bool Setup();

		void Move();

		void SetY(int y);

		int GetY();

		int GetX();

		float GetR();


	private:
		int m_x, m_y;
		float m_r;
		float m_velocity;
		sf::Texture m_texture;
		sf::Sprite* m_circle2 = nullptr;
	};

}