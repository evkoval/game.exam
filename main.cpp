﻿#include <SFML/Graphics.hpp>
#include <header.hpp>
#include <thread>
#include <chrono>
#include <vector>
#include <iostream>

using namespace std::chrono_literals;

int main()
{
    srand(time(0));

    const int width = 600;
    const int height = 800;
    const int N = 4;
    const float r = 20;
    float v=10;
    int score = 0;
    bool k = false;

    sf::RenderWindow window(sf::VideoMode(width, height), "GAME"); //Создание окна
 
    //Подгрузка шрифта
    sf::Font font;
    if (!font.loadFromFile("fonts/arial.ttf"))
    {
        std::cout << "ERROR: font was not loaded." << std::endl;
        return -1;
    }

    //Подгрузка фона
    sf::Texture texture;
    if (!texture.loadFromFile("img/back.jpg"))
    {
        std::cout << "ERROR when loading back.jpg" << std::endl;
        return false;
    }
    sf::Sprite back;
    back.setTexture(texture);

    //Создание корзины
    ek::Circle *korz= new ek::Circle(r*2, width/2, height-2*r, v); 

    //Создание яблок
    std::vector <ek::Circle2*> circles2; 
    for (int i = 0; i < N; i++)
        circles2.push_back(new ek::Circle2(r, rand()%(width/N-10)+i*(width / N), r, rand()%2 + (i+2)));
    for (const auto& circle2 : circles2)
        if (!circle2->Setup())
            return -1;

    //Счет
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(24);
    text.setFillColor(sf::Color::Red);

    //Уведомление об окончании игры
    sf::Text gameover;
    gameover.setFont(font);
    gameover.setCharacterSize(48);
    gameover.setFillColor(sf::Color::Red);
    gameover.setOutlineThickness(3);
    gameover.setOutlineColor(sf::Color::White);
    gameover.setPosition(width / 2 - 135, height / 2);
    gameover.setString(std::string("GAMEOVER"));

    while (window.isOpen())
    {
        //Установка иконки
        sf::Image icon;
        if (!icon.loadFromFile("img/apple.png"))
        {
            return -1;
        }
        window.setIcon(32, 32, icon.getPixelsPtr());

        
        sf::Event event;
        while (window.pollEvent(event))
        {
           if (event.type == sf::Event::Closed)
             window.close();
        }

        window.clear();

        window.draw(back); //Отрисовка фона
        for (const auto& circle : circles2) //Отрисовка яблок 
            window.draw(*circle->Get());
        text.setString(std::string("Score: ") + std::to_string(score)); 
        window.draw(text); //Отрисовка текста
        window.draw(*korz->Get());

        for (int i = 0; i < circles2.size(); i++) // Проверка на столкновение
        {
            int X = korz->GetX();
            int Y = korz->GetY();
            float R = korz->GetR();

            int x = circles2[i]->GetX();
            int y = circles2[i]->GetY();
            float r = circles2[i]->GetR();

            float d = sqrt((X - x) * (X - x) + (Y - y) * (Y - y));

            if (R + r >= d)
            {
                delete circles2[i];
                circles2.erase(circles2.begin() + i);
                circles2.push_back(new ek::Circle2(r, rand() % (width / N - 10) + i * (width / N), r, rand() % 2 + (i + 2))); //Добавление яблока в конец массива
                for (const auto& circle2 : circles2)
                    if (!circle2->Setup())
                        return -1;
                i--;
                score++;               
            }
        }

        //Движение корзины
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            korz->MoveKorzR();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            korz->MoveKorzL();
        }

        for (int i = 0; i < circles2.size(); i++) //Движение яблок
        {   
            circles2[i]->Move();
            if (circles2[i]->GetY() > height - circles2[i]->GetR())
            {
                window.draw(gameover);
                window.display();
                std::this_thread::sleep_for(2s);
                window.close();
                return 0;
            }
        }

        
        window.display();
        std::this_thread::sleep_for(40ms);   
    }

    for (const auto& circle : circles2) //Очистка памяти
        delete circle;
    circles2.clear();
    delete korz;


    return 0;
}
