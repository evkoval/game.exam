#include <header.hpp>


namespace ek
{
	Circle::Circle (float r, int x, int y,  float velocity) // ����������� �������
	{
		m_r = r;
		m_x = x;
		m_y = y;
		m_velocity = velocity;
		m_circle = new sf::CircleShape(m_r);
		m_circle->setOrigin(m_r, m_r);
		m_circle->setPosition(m_x, m_y);
		m_circle->setFillColor(sf::Color(135, 95, 50, 255));
		m_circle->setOutlineThickness(3);
		m_circle->setOutlineColor(sf::Color(77, 54, 28, 255));
	}

	Circle::~Circle() // ���������� �������
	{
			delete m_circle;
	}

	void Circle::Move() 
	{
		m_y += m_velocity;
		m_circle->setPosition(m_x, m_y);
	}

	void Circle::MoveKorzR() //�������� ������� ������
	{
		m_x += m_velocity;
		m_circle->setPosition(m_x, m_y);
	}

	void Circle::MoveKorzL() //�������� ������� �����
	{
		m_x -= m_velocity;
		m_circle->setPosition(m_x, m_y);
	}
	
	void Circle::SetY(int y)
	{
		m_y = y;
		m_circle->setPosition(m_x, m_y);
	}

	int Circle::GetY() { return m_y; }

	int Circle::GetX() { return m_x; }

	float Circle::GetR() { return m_r; }

	sf::CircleShape* Circle::Get() { return m_circle; }


	Circle2::Circle2(float r, int x, int y, float velocity) // ����������� �����
	{
		m_r = r;
		m_x = x;
		m_y = y;
		m_velocity = velocity;
	}

	Circle2::~Circle2() // ���������� �����
	{
		if (m_circle2 != nullptr)
			delete m_circle2;
	}



	bool Circle2::Setup() //��������� ��������
	{
		if (!m_texture.loadFromFile("img/apple.png"))
		{
			std::cout << "ERROR when loading apple.png" << std::endl;
			return false;
		}

		m_circle2 = new sf::Sprite();
		m_circle2->setTexture(m_texture);
		m_circle2->setOrigin(m_r, m_r);
		m_circle2->setPosition(m_x, m_y);
		m_circle2->setScale(0.3, 0.3);

		return true;
	}


	void Circle2::Move() //�������� �����
	{
		m_y += m_velocity;
		m_circle2->setPosition(m_x, m_y);
	}

	void Circle2::SetY(int y)
	{
		m_y = y;
		m_circle2->setPosition(m_x, m_y);
	}

	int Circle2::GetY() { return m_y; }

	int Circle2::GetX() { return m_x; }

	float Circle2::GetR() { return m_r; }

	sf::Sprite* Circle2::Get() { return m_circle2; }

}